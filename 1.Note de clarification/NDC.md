# Note de clarification

## Contexte

Dans le cadre de ce projet, nous allons développer une **application de gestion pour une clinique vétérinaire**. Cette application devra permettre à l'administrateur de gérer les éléments suivants :

- **Patients** (animaux)
- **Dossiers médicaux** des patients
- **Clients** (propriétaires des animaux)
- **Personnel soignant**
- **Administration des médicaments**

La conception du **Modèle Conceptuel des Données (MCD)** respectera les bonnes pratiques en matière d'optimisation et de maintenabilité, tout en répondant aux besoins du client.

---

## Clarification des éléments de conception

### Patient

Un **patient** (animal) possède les attributs suivants :

- Nom
- Date de naissance (peut être `NULL`)
- Espèce
- Numéro de puce d'identification (peut être `NULL`)
- Passeport (peut être `NULL`)
- Liste des propriétaires avec les périodes d'association
- Liste des vétérinaires qui l'ont suivi avec les périodes d'association

### Dossier médical

Le **dossier médical** est associé à un unique patient. Il comporte :

- Poids
- Taille
- **Entrées** :
  - Résultats d'analyses
  - Traitements prescrits
  - Observations générales faites lors des consultations
  - Procédures effectuées


> 💡 **Conseil :**  
> Chaque entrée est accompagnée de la date et l'heure de saisie ainsi que du personnel soignant en charge. 
> Chaque entrée est propre à un **dossier médical** spécifique.

### Personnel soignant

Un **personnel soignant** est défini par les attributs suivants :

- {+ Nom +}
- {+ Prénom +}
- {+ Date de naissance +}
- {+ Adresse +}
- {+ Numéro de téléphone +}
- Poste 
- Spécialité

### Client

Un **client** (propriétaire) est défini par les mêmes attributs que le personnel soignant :

- {+ Nom +}
- {+ Prénom +}
- {+ Date de naissance +}
- {+ Adresse +}
- {+ Numéro de téléphone +}

> ⚠️ **Attention :**  
> Il existe une **redondance** entre les attributs du personnel soignant et ceux des clients. Cela constituera une piste d'optimisation pour la conception ultérieure.

> 💡 **Conseil :**  
> Une personne ne peut pas être à la fois un **client** et un **personnel soignant**. Autrement dit, un **animal** ne peut avoir pour propriétaire un personnel soignant.

### Médicament

Un **médicament** est caractérisé par :

- Nom de la molécule
- Ensemble d'effets
- Liste des espèces autorisées pour ce médicament
