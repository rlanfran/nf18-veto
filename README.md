# NF18 - Veto

## Gestion d'une clinique vétérinaire

Dans le cadre de notre projet nous allons concevoir un système de **gestion de base de donnée** et une application qui servira d'interface pour une clinique vétérinaire.

### TD2 - groupe 3
- Yasmine Benjouad
- Noah Maréchal
- Albéric Peveraud De Vaumas
- Robin Lanfranchi

### Organisation des rendus
#### rendu 1 du 09/10 
- La NDC sera disponible au format markdown sous forme de fichier dans le projet GIT nommé **1.Note de clairfication/NDC.md**
- Un premier UML au format plantuml sera contenu dans un fichier **2.Modèle conceptuel données/UML.md**

#### rendu 2 du 16/10
En plus de la NDC, seront disponibles :
- Une version corrigée du MCD en UML au format plantuml  
- Une première version du MLD

#### rendu 3 du 15/11
En plus de la NDC, seront disponibles :
- Une version actualisée du MCD en UML, dans des fichiers **uml_3.png** et **UML.plantuml**
- Une version corrigée du MLD dans un fichier **MLD.txt**
- Une première version des codes SQL dans un fichier **table.sql**

